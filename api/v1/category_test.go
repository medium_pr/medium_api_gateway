package v1_test

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/medium_pr/medium_api_gateway/api/models"
)

func createCategory(t *testing.T) *models.Category {
	authSuperadmin := loginSuperadmin(t)

	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.Category{
		Title: faker.Sentence(),
	})
	assert.NoError(t, err)
	req, _ := http.NewRequest("POST", "/v1/categories", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", authSuperadmin.AccessToken)

	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusCreated, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.Category
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	return &response
}

func TestCreateCategory(t *testing.T) {
	createCategory(t)
}
