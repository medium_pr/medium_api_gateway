package v1_test

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/medium_pr/medium_api_gateway/api/models"
)

func createPost(t *testing.T) *models.Post {
	authUser := loginUser(t)
	category := createCategory(t)

	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.Post{
		Title:       faker.Sentence(),
		Description: faker.Sentence(),
		ImageUrl:    faker.URL(),
		UserID:      authUser.ID,
		CategoryID:  category.ID,
	})
	assert.NoError(t, err)
	req, _ := http.NewRequest("POST", "/v1/posts", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", authUser.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusCreated, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.AuthResponse
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	return nil
}

func TestCreatePost(t *testing.T) {
	createPost(t)
}
