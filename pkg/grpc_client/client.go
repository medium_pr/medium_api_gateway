package grpc_client

import (
	"fmt"

	"gitlab.com/medium_pr/medium_api_gateway/config"
	pbp "gitlab.com/medium_pr/medium_api_gateway/genproto/post_service"
	pbu "gitlab.com/medium_pr/medium_api_gateway/genproto/user_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

//go:generate mockgen -source ../../genproto/user_service/user_service_grpc.pb.go -package mock_grpc -destination ./mock_grpc/user_service_grpc.gen.go
//go:generate mockgen -source ../../genproto/user_service/auth_service_grpc.pb.go -package mock_grpc -destination ./mock_grpc/auth_service_grpc.gen.go

type GrpcClientI interface {
	UserService() pbu.UserServiceClient
	SetUserService(u pbu.UserServiceClient)
	AuthService() pbu.AuthServiceClient
	SetAuthService(u pbu.AuthServiceClient)
	PostService() pbp.PostServiceClient
	CategoryService() pbp.CategoryServiceClient
}

type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

func New(cfg config.Config) (GrpcClientI, error) {
	connUserService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.UserServiceHost, cfg.UserServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("user service dial host: %s port:%s err: %v",
			cfg.UserServiceHost, cfg.UserServiceGrpcPort, err)
	}

	connPostService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.PostServiceHost, cfg.PostServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("post service dial host: %s port:%s err: %v",
			cfg.PostServiceHost, cfg.PostServiceGrpcPort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"user_service":     pbu.NewUserServiceClient(connUserService),
			"auth_service":     pbu.NewAuthServiceClient(connUserService),
			"post_service":     pbp.NewPostServiceClient(connPostService),
			"category_service": pbp.NewCategoryServiceClient(connPostService),
		},
	}, nil
}

func (g *GrpcClient) UserService() pbu.UserServiceClient {
	return g.connections["user_service"].(pbu.UserServiceClient)
}

func (g *GrpcClient) AuthService() pbu.AuthServiceClient {
	return g.connections["auth_service"].(pbu.AuthServiceClient)
}

func (g *GrpcClient) PostService() pbp.PostServiceClient {
	return g.connections["post_service"].(pbp.PostServiceClient)
}

func (g *GrpcClient) CategoryService() pbp.CategoryServiceClient {
	return g.connections["category_service"].(pbp.CategoryServiceClient)
}
