package grpc_client

import pbu "gitlab.com/medium_pr/medium_api_gateway/genproto/user_service"

func (g *GrpcClient) SetUserService(u pbu.UserServiceClient) {
	g.connections["user_service"] = u
}

func (g *GrpcClient) SetAuthService(a pbu.AuthServiceClient) {
	g.connections["auth_service"] = a
}
